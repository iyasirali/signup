//
//  CongratzViewController.swift
//  Signup
//
//  Created by Yasir ali on 01/12/2021.
//

import UIKit
import Reusable
import Utilities

class CongratzViewController: BaseViewController,StoryboardSceneBased, ViewModelBased {
    static var sceneStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: .main)
    
    //MARK: Outlets
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var btnSignup: UIButton!
    
    //MARK: Properties
    var viewModel: CongratzViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       bindViewModel()
    }
    
    private func bindViewModel() {
        bindInput()
        bindOutput()
    }
    
    private func bindInput() {
        btnSignup.rx.tap.bind(to: viewModel.input.didTapSignup).disposed(by: disposeBag)
    }
    
    private func bindOutput() {
        viewModel.output.message.bind(to: labelMessage.rx.text).disposed(by: disposeBag)
    }
}
