//
//  CongratzViewModel.swift
//  Signup
//
//  Created by Yasir ali on 01/12/2021.
//

import Foundation
import RxSwift
import Store
import Utilities
import RxFlow
import RxRelay
import Moya

class CongratzViewModel: ViewModel, Stepper, ServiceBased {
    
    
    
    
    struct Input {
        let didTapSignup: AnyObserver<Void>
    }
    
    
    struct Output {
        let message: Observable<String>
    }
    
    var input: Input
    var output: Output
    
    let disposeBag = DisposeBag()
    
    var steps: PublishRelay<Step>
    
    typealias ServiceType = AuthDependency
    var services: AuthDependency
    
    
    init(dependency: AuthDependency) {
        self.services = dependency
        steps = PublishRelay()
       
        //MARK: Input
        let didTapSignup = PublishSubject<Void>()
       
        input = Input(didTapSignup: didTapSignup.asObserver() )
        
       
        //MARK: Output
        let messsage = ReplaySubject<String>.create(bufferSize: 1)
       
        
        output = Output(message: messsage.asObservable())
        
        messsage.onNext("Congratz! You have created an account")
        
        didTapSignup.subscribe (onNext: { [weak self] (_) in
            //self?.triggerStep(AuthStep.)
        }).disposed(by: disposeBag)
    }
    
   
    //MARK: TriggerStep
    private func triggerStep(_ step: Step) {
        self.steps.accept(step)
    }

}
