//
//  SignupViewController.swift
//  Signup
//
//  Created by Yasir ali on 30/11/2021.
//

import UIKit
import Reusable
import Utilities

class SignupViewController: BaseViewController,StoryboardSceneBased, ViewModelBased {
    static var sceneStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: .main)
    
    //MARK: Outlets
    
    @IBOutlet weak var nameTextField: PrimaryTextField!
    @IBOutlet weak var emailTextField: PrimaryTextField!
    @IBOutlet weak var phoneTextField: PrimaryTextField!
    @IBOutlet weak var btnSignup: UIButton!
    
    //MARK: Properties
    var viewModel: SignupViewModel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        bindViewModel()
    }
    
    //MARK: UI
    private func configUI() {
        progressView = SimpleProgressView(frame: .zero)
        nameTextField.title = "Full Name"
        nameTextField.placeholder = "Full Name"
        
        emailTextField.title = "Email"
        emailTextField.placeholder = "Email"
        
        phoneTextField.title = "Mobile"
        phoneTextField.placeholder = "+923211234567"
    }
    
    private func bindViewModel() {
        bindInput()
        bindOutput()
        bindLoading()
    }
    
    private func bindInput() {
        nameTextField.rx.text.orEmpty.bind(to: viewModel.input.fullName).disposed(by: disposeBag)
        emailTextField.rx.text.orEmpty.bind(to: viewModel.input.email).disposed(by: disposeBag)
        phoneTextField.rx.text.orEmpty.bind(to: viewModel.input.phoneNo).disposed(by: disposeBag)
      
        btnSignup.rx.tap.bind(to: viewModel.input.didTapSignup).disposed(by: disposeBag)
    }
    
    private func bindOutput() {
        viewModel.output.nameStatus.bind(to: nameTextField.validationState).disposed(by: disposeBag)
        viewModel.output.emailStatus.bind(to: emailTextField.validationState).disposed(by: disposeBag)
        viewModel.output.phoneStatus.bind(to: phoneTextField.validationState).disposed(by: disposeBag)
      
        
        viewModel.output.isSignupEnabled.bind(to: btnSignup.rx.isEnabled).disposed(by: disposeBag)
        viewModel.output.onShowSuccess.subscribe(onNext: { [weak self] (msg) in
            guard let self = self else { return }
            self.view.showSnackBarMessage(msg: msg, type: .success)
        }).disposed(by: disposeBag)
        
       
        
        viewModel.output.onShowInfo.subscribe(onNext: {  [weak self] (msg) in
            guard let self = self else { return }
            self.view.showSnackBarMessage(msg: msg, type: .warning)
        }).disposed(by: disposeBag)
        
        viewModel.output.onShowError.subscribe(onNext: {  [weak self] (msg) in
            guard let self = self else { return }
            self.view.showSnackBarMessage(msg: msg, type: .error)
        }).disposed(by: disposeBag)
    }
    
    private func bindLoading() {
        viewModel.output.onShowLoading.subscribe(onNext: { [weak self] (isShow) in
            guard let self = self else { return }
            if isShow {
               // self.showHud("")
                _  = self.progressView?.show(fromViewController: self, insets: .zero, animated: true, completion: nil)
            } else {
                //self.hideHUD()
                self.progressView?.hide(animated: false, completion: nil)
            }
        }).disposed(by: disposeBag)
    }
    

}
