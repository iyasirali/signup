//
//  SignupViewModel.swift
//  Signup
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation
import RxSwift
import Store
import Utilities
import RxFlow
import RxRelay
import Moya

class SignupViewModel: ViewModel, Stepper, ServiceBased {
    
    
    
    
    struct Input {
        let fullName: AnyObserver<String>
        let email: AnyObserver<String>
        let phoneNo: AnyObserver<String>
        let didTapSignup: AnyObserver<Void>
    }
    
    
    struct Output {
        let nameStatus: Observable<ValidationState>
        let emailStatus: Observable<ValidationState>
        let phoneStatus: Observable<ValidationState>
        let isSignupEnabled: Observable<Bool>
        
        let onShowLoading = PublishSubject<Bool>()
        let onShowError = PublishSubject<String>()
        let onShowSuccess = PublishSubject<String>()
        let onShowInfo = PublishSubject<String>()
    }
    
    var input: Input
    var output: Output
    
    let disposeBag = DisposeBag()
    private let emailValidator = VaildatorFactory.validatorFor(type: .email)
    private let phoneValidator = VaildatorFactory.validatorFor(type: .phoneNo)
    private var nameValidator = VaildatorFactory.validatorFor(type: .name)
    
    var steps: PublishRelay<Step>
    
    typealias ServiceType = AuthDependency
    var services: AuthDependency
    
    
    init(dependency: AuthDependency) {
        self.services = dependency
        steps = PublishRelay()
       
        //MARK: Input
       
        let fullName = PublishSubject<String>()
        let email = PublishSubject<String>()
        let phoneNo = PublishSubject<String>()
        let didTapSignup = PublishSubject<Void>()
       
        input = Input(fullName: fullName.asObserver(), email: email.asObserver(), phoneNo: phoneNo.asObserver(), didTapSignup: didTapSignup.asObserver())
        
        fullName.bind(to: nameValidator.text).disposed(by: disposeBag)
        email.bind(to: emailValidator.text).disposed(by: disposeBag)
       // mobileNo.bind(to: phoneValidator.text).disposed(by: disposeBag)
        phoneNo.map { (number) -> String in
            return (number.count < 13) ? number : String(number.prefix(13))
        }.bind(to: phoneValidator.text).disposed(by: disposeBag)

        
        //MARK: Output
        let nameStatus = PublishSubject<ValidationState>()
        let emailStatus = PublishSubject<ValidationState>()
        let phoneStatus = PublishSubject<ValidationState>()
        
        let isEnabledRegister = ReplaySubject<Bool>.create(bufferSize: 1)
        
        output = Output(nameStatus: nameStatus.asObservable(), emailStatus: emailStatus.asObservable(), phoneStatus: phoneStatus.asObservable(), isSignupEnabled: isEnabledRegister.asObservable())
        
        
        nameValidator.state.bind(to: nameStatus).disposed(by: disposeBag)
        emailValidator.state.bind(to: emailStatus).disposed(by: disposeBag)
        phoneValidator.state.bind(to: phoneStatus).disposed(by: disposeBag)
        
        Observable.combineLatest(output.nameStatus,output.emailStatus,
                                 output.phoneStatus) {  (userNameState: ValidationState,emailState: ValidationState, phoneState: ValidationState) -> Bool in
        switch (userNameState,emailState, phoneState) {
        case (.valid,.valid,.valid):
           return true
        default:
            return false
        }
        }
        .bind(to: isEnabledRegister)
        .disposed(by: disposeBag)
        
        let inputCombine = Observable.combineLatest(fullName,email,phoneNo)
        
        didTapSignup.withLatestFrom(inputCombine).subscribe (onNext: { [weak self] (_arg1) in
            let (name,email,phn) = _arg1
            let input = SignupInput(email: email, phone: phn, name: name)
            self?.signup(input: input)
        }).disposed(by: disposeBag)
    }
    
    //MARK: Parent
    private func signup(input: SignupInput) {
        output.onShowLoading.onNext(true)
        self.services.authRepo.signup(input: input).subscribe(onSuccess: { [weak self] (signupResponse) in
            guard let self = self else { return }
            self.output.onShowLoading.onNext(false)
            if let msg = signupResponse.message, let _ = signupResponse.data {
                self.output.onShowSuccess.onNext(msg)
                self.triggerStep(AuthStep.congratulations)
            } else {
                self.output.onShowInfo.onNext(signupResponse.message ?? "")
            }
            
            
        }) { [weak self] ( error) in
            guard let self = self else { return }
            self.output.onShowLoading.onNext(false)
            
            if let moyaError = error as? MoyaError {
                switch  moyaError {
                case .underlying(let therror, _):
                    if let lmsrr = therror as? LMSError {
                        switch lmsrr {
                        case .customError(customError: let errModel):
                            
                            self.output.onShowError.onNext(errModel.Message ?? "Found an error")
                        }
                    }
                
                default:
                    break
                }
                
            }
            
            
            
        }.disposed(by: disposeBag)
    }
    
    //MARK: TriggerStep
    private func triggerStep(_ step: Step) {
        self.steps.accept(step)
    }

}
