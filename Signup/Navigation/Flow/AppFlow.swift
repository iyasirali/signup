//
//  AppFlow.swift
//  Signup
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation
import RxFlow
import RxRelay
import Store
import RxSwift

enum AppStep: Step {
    case auth
    case home
}

class AppStepper: Stepper {
    
    var steps = PublishRelay<Step>()
    var initialStep: Step = AppStep.auth
    
    init() {}
}

class AppFlow: Flow {
    var root: Presentable {
        return window
    }
    
    private let window: UIWindow!
    let authRepo: AuthRepository
    let disposeBag = DisposeBag()
    
    init(withWindow window: UIWindow) {
        self.window = window
        window.rootViewController = UIViewController() //rootViewController
        authRepo = AuthRepository(remote: AuthRemoteSource())
        
    }
    
    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppStep else { return .none }
        switch step {
        case .auth:
            return navigateToAuthFlow()
            
        case .home:
            
            return .none
        }
    }
    
    //MARK: Start AuthFlow
    private func navigateToAuthFlow()  -> FlowContributors{
        
        let flow = AuthFlow(authDependency: AuthDependency(authRepo: authRepo))
        Flows.use(flow,when: .ready) { [unowned self] (root) in
            self.window.rootViewController = root
            self.window.makeKeyAndVisible()
        }
        let oneStep =  AuthStepper() //OneStepper(withSingleStep: AuthStep.login)
        let flowContributor = FlowContributor.contribute(withNextPresentable: flow, withNextStepper: oneStep)
        return FlowContributors.one(flowContributor: flowContributor)
    
    }
}
