//
//  AuthFlow.swift
//  Signup
//
//  Created by Yasir ali on 30/11/2021.
//

import UIKit
import Store
import RxFlow
import RxRelay


enum AuthStep: Step {
    case signup
    case congratulations
    case goToSignup
}

struct AuthDependency: HasAuthRepo {
    let authRepo: AuthRepository
}

class AuthStepper: Stepper {
    var steps = PublishRelay<Step>()
    
    var initialStep: Step = AuthStep.signup
    init() {}
}

class AuthFlow: Flow {
    var root: Presentable { rootViewController }
    
    let rootViewController: UINavigationController = {
        let scene = UINavigationController()
        scene.isNavigationBarHidden = true
        return scene
    }()
    
    private  let dependency: AuthDependency
    
    
    init(authDependency: AuthDependency) {
        self.dependency = authDependency
    }
    
    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AuthStep else { return .none }
        switch step {
        
        case .signup:
            return navigateToSignupScene()
        case .congratulations:
            return navigateToCongratzScene()
        case .goToSignup:
            return .none
        }
    }
    
    //MARK: Signup Scene
    private func navigateToSignupScene() -> FlowContributors {
        let viewModel = SignupViewModel(dependency: dependency)
        let vc = SignupViewController.instantiate(withViewModel: viewModel)
       
        rootViewController.setViewControllers([vc], animated: true)
        return .one(flowContributor: FlowContributor.contribute(withNextPresentable: vc, withNextStepper: viewModel))
    }
    
    //MARK: Congratz Scene
    private func navigateToCongratzScene() -> FlowContributors {
        let viewModel = CongratzViewModel(dependency: dependency)
        let vc = CongratzViewController.instantiate(withViewModel: viewModel)
       
        rootViewController.pushViewController(vc, animated: true)
        return .one(flowContributor: FlowContributor.contribute(withNextPresentable: vc, withNextStepper: viewModel))
    }
}
