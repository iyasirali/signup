//
//  UIView+custom.swift
//  Signup
//
//  Created by Yasir ali on 01/12/2021.
//

import UIKit
import Utilities
import MaterialComponents.MaterialSnackbar_TypographyThemer

public extension UIView {
    enum SnackBarMessageType {
        case success
        case info
        case error
        case warning
    }
    
    func showSnackBarMessage(msg: String, type: SnackBarMessageType, isBottomOffset: Bool = false, offset: CGFloat = 0) {
        let message = MDCSnackbarMessage()
        
        //let typographyScheme = MDCTypographyScheme()
        
        switch type {
        case .success:
            MDCSnackbarManager.snackbarMessageViewBackgroundColor =   .green
        case .info:
            MDCSnackbarManager.snackbarMessageViewBackgroundColor =   .gray
        case .error:
            MDCSnackbarManager.snackbarMessageViewBackgroundColor =   .red
        case .warning:
            MDCSnackbarManager.snackbarMessageViewBackgroundColor =   .yellow
        }
        
        
        message.text = msg
        
        //message.automaticallyDismisses = true
        if isBottomOffset {
             MDCSnackbarManager.setBottomOffset(offset)
        }
       
        MDCSnackbarManager.show(message)
    }
    
    
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    // bottom
    func roundCorners(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    // top
    func topCorners(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func roundCornersAll(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner,.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func dropShadow(scale: Bool = true) {
       // layer.masksToBounds = false
        /*layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 1
        
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1 */
        
        //let shadowPath = UIBezierPath(rect: self.bounds)
        //layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 4.0
        layer.shadowOffset = CGSize(width: 3.0, height: 3.0)// CGSCGize(0.0, 5.0)
        layer.shadowOpacity = 0.7
        //layer.shadowPath = shadowPath.cgPath
    }
    
    func addTopBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }
}

