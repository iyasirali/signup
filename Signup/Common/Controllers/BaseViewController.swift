//
//  BaseViewController.swift
//  Signup
//
//  Created by Yasir ali on 01/12/2021.
//

import Foundation
import Utilities
import RxSwift

import UIKit
import RxSwift
import RxCocoa

public enum SystemTheme {
    @available(iOS 12.0, *)
    static func get(on view: UIView) -> UIUserInterfaceStyle {
        view.traitCollection.userInterfaceStyle
    }

    @available(iOS 12.0, *)
    static func observe(on view: UIView) -> Observable<UIUserInterfaceStyle> {
        view.rx.methodInvoked(#selector(UIView.traitCollectionDidChange(_:)))
            .map { _ in SystemTheme.get(on: view) }
            .distinctUntilChanged()
    }
    
    @available(iOS 12.0, *)
    static var osTheme: UIUserInterfaceStyle {
        return UIScreen.main.traitCollection.userInterfaceStyle
    }
}


open class BaseViewController: UIViewController {
    
    public var errorView: ViewErrorDisplayable?
    public var progressView: ViewDisplayable?
    public let disposeBag = DisposeBag()

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //return ThemeManager.shared.currentTheme.navBarStyle.lightStyle.statusBarStyle
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        createResponseViews()
        
        
       NotificationCenter.default.addObserver(self,
        selector: #selector(applicationDidBecomeActive),
        name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
        name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func applicationDidBecomeActive() {
        let appState = UIApplication.shared.applicationState
        switch appState {
        case .active:
            print("active")
            if #available(iOS 12.0, *) {
                setColors()
            } else {
                // Fallback on earlier versions
            }
        case .background:
            print("background")
        case .inactive:
            print("inActive")
        }
    }
    
//    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//        super.traitCollectionDidChange(previousTraitCollection)
//
//
//    }

    /// Create progress and error view for api responses
    open func createResponseViews() {
        progressView = SimpleProgressView(frame: .zero)
        errorView = SimpleErrorView(frame: .zero)
    }
    
    @available(iOS 12.0, *)
    func setColors() {
        let userInterfaceStyle = SystemTheme.osTheme
        switch userInterfaceStyle {
        case .dark:
            print("dark")
        case .light:
            print("light")
        case .unspecified:
            print("unspecified")
        }
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        //let uiStyle = window.traitCollection.userInterfaceStyle
        switch userInterfaceStyle {
        case .dark, .unspecified:
            if #available(iOS 13.0, *) {
                window.overrideUserInterfaceStyle = .dark
                changeNavigatorBar(forDark: true)
                print("dark mode enabled")
            } else {
                // Fallback on earlier versions
            }
        case .light:
            if #available(iOS 13.0, *) {
                changeNavigatorBar(forDark: false)
                window.overrideUserInterfaceStyle = .light
                print("light mode enabled")
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
    
    private func changeNavigatorBar(forDark: Bool) {
        if forDark {
            
            /*UINavigationBar.appearance().barTintColor = .black
            UINavigationBar.appearance().tintColor = .black
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            UINavigationBar.appearance().isTranslucent = true */
           // UINavigationBar.appearance().backgroundColor = .black //ThemeManager.shared.currentTheme.color.secondaryText
        } else {
          //  UINavigationBar.appearance().backgroundColor = .white
           /* UINavigationBar.appearance().barTintColor = .white
            UINavigationBar.appearance().tintColor = .black
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            UINavigationBar.appearance().isTranslucent = false */
        }
    }
}



