//
//  PrimaryTextField.swift
//  Signup
//
//  Created by Yasir ali on 01/12/2021.
//

import UIKit
import RxCocoa
import RxSwift
import Utilities
import SkyFloatingLabelTextField


final public class PrimaryTextField: SkyFloatingLabelTextField {
    
    
    //MARK: ValidatableField
    public var validationState = BehaviorRelay<ValidationState>(value: .empty)
    
   
    var padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    let disposeBag = DisposeBag()
    
    let isSecureEntryEnabled = ReplaySubject<Bool>.create(bufferSize: 1)
    
    //MARK: Lifecycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        customizeUI()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        customizeUI()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
//    override public func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }

    //MARK: UI
    func customizeUI() {
//        self.clipsToBounds = true
//        self.textColor = UIColor.black
       
        
        
    }
   
}

