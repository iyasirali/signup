//
//  AppDelegate.swift
//  Signup
//
//  Created by Yasir ali on 29/11/2021.
//

import UIKit
import RxFlow
import RxSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let disposeBag = DisposeBag()
    let coordinator = FlowCoordinator()
    var appFlow: AppFlow!
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        coordinator.rx.didNavigate.subscribe(onNext: { (flow,step) in
            print ("did navigate to flow=\(flow) and step=\(step)")
        }).disposed(by: disposeBag)
        
        appFlow = AppFlow(withWindow: window!)
        let stepper = AppStepper()
        coordinator.coordinate(flow: appFlow, with: stepper)
        
        return true
    }



}

