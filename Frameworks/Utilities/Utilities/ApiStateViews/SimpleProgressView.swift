//
//  SimpleProgressView.swift
//  BeatTheQueue
//
//  Created by Yasir Ali on 15/12/2018.
//  Copyright © 2018 Yasir Ali. All rights reserved.
//

import UIKit

public enum IndicatorType {
    case medium
    case white
    case grey
    case whiteLarge
}

/// Displays an activity indicator view at the center
 public class SimpleProgressView: UIView {
    
    
	//MARK: Properties
    public let activityIndicatorView: UIActivityIndicatorView = {
		let indicator = UIActivityIndicatorView() //UIActivityIndicatorView(style: .gray)
        //indicator.tintColor = UIColor(hexString: "#00B8F4")
        if #available(iOS 13.0, *) {
            // use the feature only available in iOS 9
            // for ex. UIStackView
            
            
             //indicator.style = .whiteLarge
            indicator.style = .medium
        } else {
            indicator.style = .gray //.whiteLarge
        }
        //.whiteLarge
        //indicator.color = UIColor(hexString: "#00B8F4")
		indicator.hidesWhenStopped = true
		return indicator
	}()

	//MARK: Lifecycle
	override public init(frame: CGRect) {
		super.init(frame: frame)
		customizeUI()
	}

	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		customizeUI()
	}

	//MARK: UI

	/// Construct UI
    public func customizeUI() {

		addSubViews(activityIndicatorView)
		setupLayout()
	}


	/// Add Constraints to subviews
    public func setupLayout() {
		let parent = self
		NSLayoutConstraint.activate([
			activityIndicatorView.centerXAnchor.constraint(equalTo: parent.centerXAnchor),
			activityIndicatorView.centerYAnchor.constraint(equalTo: parent.centerYAnchor)
			])
	}

}

//MARK: View Animatable
extension SimpleProgressView: ViewAnimatable {

	public var animationDuration: Double { return 0.3 }

	public var isAnimating: Bool {
		return activityIndicatorView.isAnimating
	}

	public func playAnimation() {
		activityIndicatorView.startAnimating()
	}

	public func stopAnimation() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
        }
	}
}


//MARK: ViewDisplayable
extension SimpleProgressView: ViewDisplayable {


	public var viewTransitionDuration: Double {
		return 0.3
	}

	/// Add view and display view on view controller
	///
	/// - Parameters:
	///   - viewController: controller in which view will be displayed
	///   - insets: margins from edges
	///   - animated: with fade animation if true
	///   - completion: callback when view is displayed
	public func show(fromViewController viewController: UIViewController, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

		add(to: viewController.view, withInsets: insets)
		presentView(animated: animated) { [weak self] (finished) in
            if finished{
				self?.playAnimation()
            }
			completion?(finished)
		}

	}

	/// Add view and display view on another view
	///
	/// - Parameters:
	///   - view: parent view on which view will be displayed
	///   - insets: margins from edges
	///   - animated: with fade animation if true
	///   - completion: callback when view is displayed
	public func show(fromView view: UIView, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

		add(to: view, withInsets: insets)
		presentView(animated: animated) { [weak self] (finished) in
			if finished{
				self?.playAnimation()
			}
			completion?(finished)
		}
	}


	/// Hide and remove view from superview
	///
	/// - Parameters:
	///   - animated: with fade animation if true
	///   - completion: callback when view is hidden and removed
	public func hide(animated: Bool, completion: ((Bool) -> Void)?) {
		self.stopAnimation()
		self.dismissView(animated: animated, completion: completion)
	}
}

final public class CenteredProgressView: SimpleProgressView {
    
    private let centerView = UIView()
    
    
    override public func customizeUI() {
        super.customizeUI()
        
        backgroundColor = UIColor.black.withAlphaComponent(0.4)
        centerView.backgroundColor = UIColor.white
        centerView.layer.cornerRadius = 10
//        centerView.layer.borderWidth = 1
//        centerView.layer.borderColor = UIColor(hexString: "#00B8F4").cgColor
        self.add(indicator: centerView, to: self)
        activityIndicatorView.tintColor = .darkGray
        self.bringSubviewToFront(activityIndicatorView)
//       let thisActivityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//        thisActivityIndicator.style = .gray
//        thisActivityIndicator.tintColor = .red
//        self.addActivityIndicator(indicator: thisActivityIndicator, to: centerView)
    }
    
    private func add(indicator child: UIView, to parent: UIView) {
        child.translatesAutoresizingMaskIntoConstraints = false
        parent.addSubview(child)
        
        child.heightAnchor.constraint(equalToConstant: 100).isActive = true
        child.widthAnchor.constraint(equalToConstant: 100).isActive = true
        child.centerXAnchor.constraint(equalTo: parent.centerXAnchor).isActive = true
        child.centerYAnchor.constraint(equalTo: parent.centerYAnchor).isActive = true
    }
    
//    private func addActivityIndicator(indicator child: UIView, to parent: UIView) {
//        child.translatesAutoresizingMaskIntoConstraints = false
//        parent.addSubview(child)
//
//        child.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        child.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        child.centerXAnchor.constraint(equalTo: parent.centerXAnchor).isActive = true
//        child.centerYAnchor.constraint(equalTo: parent.centerYAnchor).isActive = true
//    }
//
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.dropShadow(view: centerView,color: UIColor.black, opacity: 0, offSet: CGSize(width: 0, height: 0), radius: 10)
    }
    
    private func dropShadow(view: UIView , color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        
        view.layer.masksToBounds = false
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = offSet
        view.layer.shadowRadius = radius
        
        view.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        // layer.shouldRasterize = true
        // to avoid blurred content if scale is true
        view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        view.layer.shouldRasterize = true
    }
}


/// Displays an activity indicator view at the center
 public class SimpleProgressViewWithIndicotrStyle: UIView {
    
    
    //MARK: Properties
    public let activityIndicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView() //UIActivityIndicatorView(style: .gray)
        //indicator.tintColor = UIColor(hexString: "#00B8F4")
        if #available(iOS 13.0, *) {
            // use the feature only available in iOS 9
            // for ex. UIStackView
            
            
             //indicator.style = .whiteLarge
            indicator.style = .medium
        } else {
            indicator.style = .gray //.whiteLarge
        }
        //.whiteLarge
        //indicator.color = UIColor(hexString: "#00B8F4")
        indicator.hidesWhenStopped = true
        return indicator
    }()

    //MARK: Lifecycle
    override public init(frame: CGRect) {
        super.init(frame: frame)
        customizeUI()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customizeUI()
    }

    //MARK: UI

    /// Construct UI
    public func customizeUI() {

        addSubViews(activityIndicatorView)
        setupLayout()
    }


    /// Add Constraints to subviews
    public func setupLayout() {
        let parent = self
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: parent.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: parent.centerYAnchor)
            ])
    }

}

//MARK: View Animatable
extension SimpleProgressViewWithIndicotrStyle: ViewAnimatable {

    public var animationDuration: Double { return 0.3 }

    public var isAnimating: Bool {
        return activityIndicatorView.isAnimating
    }

    public func playAnimation() {
        activityIndicatorView.style = .white
        activityIndicatorView.startAnimating()
    }

    public func stopAnimation() {
        activityIndicatorView.stopAnimating()
    }
}


//MARK: ViewDisplayable
extension SimpleProgressViewWithIndicotrStyle: ViewDisplayable {


    public var viewTransitionDuration: Double {
        return 0.3
    }

    /// Add view and display view on view controller
    ///
    /// - Parameters:
    ///   - viewController: controller in which view will be displayed
    ///   - insets: margins from edges
    ///   - animated: with fade animation if true
    ///   - completion: callback when view is displayed
    public func show(fromViewController viewController: UIViewController, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

        add(to: viewController.view, withInsets: insets)
        presentView(animated: animated) { [weak self] (finished) in
            if finished{
                self?.playAnimation()
            }
            completion?(finished)
        }

    }

    /// Add view and display view on another view
    ///
    /// - Parameters:
    ///   - view: parent view on which view will be displayed
    ///   - insets: margins from edges
    ///   - animated: with fade animation if true
    ///   - completion: callback when view is displayed
    public func show(fromView view: UIView, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

        add(to: view, withInsets: insets)
        presentView(animated: animated) { [weak self] (finished) in
            if finished{
                self?.playAnimation()
            }
            completion?(finished)
        }
    }


    /// Hide and remove view from superview
    ///
    /// - Parameters:
    ///   - animated: with fade animation if true
    ///   - completion: callback when view is hidden and removed
    public func hide(animated: Bool, completion: ((Bool) -> Void)?) {
        self.stopAnimation()
        self.dismissView(animated: animated, completion: completion)
    }
}
