//
//  ReloadableErrorView.swift
//  BeatTheQueue
//
//  Created by Yasir Ali on 09/12/2018.
//  Copyright © 2018 Yasir Ali. All rights reserved.
//

import UIKit

/// Display image, title and retry action
final public class SimpleErrorView: UIView, ViewErrorDisplayable {

	//MARK: ViewReloadable
	public var actionCallback: ActionCallBack?

	//MARK: View Animatable
	/// Animation duration for showing and hiding view if animated is true while calling show or hide func
	/*public var animationDuration: Double = 0.3

	public var isAnimating: Bool {
		return false
	}

	public func playAnimation() { }

	public func stopAnimation() { }*/

	//MARK: Properties

	public let backgroundImageView: UIImageView = {
		let imgView = UIImageView()
		imgView.contentMode = .scaleAspectFit
		return imgView
	}()

	private let container: UIView = {
		let view = UIView(frame: .zero)
		view.clipsToBounds = true
		return view
	}()

	/// Image Didsplayed Above message
	public let msgImageView: UIImageView = {
		let imgView = UIImageView()
		imgView.contentMode = .scaleAspectFit
		return imgView
	}()

	/// Message Label
	public let messageLabel: UILabel = {
		let label = UILabel()
		label.text = ""
		label.textAlignment = .center
		label.numberOfLines = 3
		label.font = UIFont.systemFont(ofSize: 14)
		return label
	}()

	public let action: UIButton = {
		let action = UIButton(type: .system)
		//		action.showTestBorder()
		action.setTitle("Reload", for: .normal)
		action.tintColor = UIColor.white
		action.titleLabel?.font = UIFont.systemFont(ofSize: 14)
		return action
	}()

	//MARK: Lifecycle
	override public init(frame: CGRect) {
        super.init(frame: frame)
		customizeUI()
	}

	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
        customizeUI()
	}

	//MARK: UI

	/// Construct UI
	private  func customizeUI() {

		self.backgroundColor = UIColor.white

		addSubViews(backgroundImageView,
					container)

		container.addSubViews(msgImageView,
							  messageLabel,
							  action)

		setupLayout()
	}


	/// Add Constraints to subviews
	private func setupLayout() {

		//Background Image View
		NSLayoutConstraint.activate([
			backgroundImageView.topAnchor.constraint(equalTo: self.topAnchor),
			backgroundImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
			self.trailingAnchor.constraint(equalTo: backgroundImageView.trailingAnchor),
			self.bottomAnchor.constraint(equalTo: backgroundImageView.bottomAnchor)
			])

		//Container
		NSLayoutConstraint.activate([
			backgroundImageView.topAnchor.constraint(equalTo: self.topAnchor),
			self.bottomAnchor.constraint(equalTo: backgroundImageView.bottomAnchor),
			backgroundImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
			backgroundImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
			])

		let parent = container

		//Message Image View
		NSLayoutConstraint.activate([
			msgImageView.topAnchor.constraint(equalTo: parent.topAnchor),
			msgImageView.leadingAnchor.constraint(equalTo: parent.leadingAnchor, constant: 64),
			parent.trailingAnchor.constraint(equalTo: msgImageView.trailingAnchor, constant: 64),
			msgImageView.heightAnchor.constraint(equalToConstant: 200),
			msgImageView.heightAnchor.constraint(greaterThanOrEqualToConstant: 80),
			])

		//Message Label
		NSLayoutConstraint.activate([
			messageLabel.topAnchor.constraint(equalTo: msgImageView.topAnchor, constant: 24),
			messageLabel.leadingAnchor.constraint(equalTo: msgImageView.leadingAnchor),
			msgImageView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor)
			])

		//Action
		NSLayoutConstraint.activate([
			action.heightAnchor.constraint(equalToConstant: 44),
			action.widthAnchor.constraint(equalToConstant: 160),
			action.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 44),
			parent.bottomAnchor.constraint(equalTo: action.bottomAnchor),
			action.centerXAnchor.constraint(equalTo: parent.centerXAnchor)
			])
	}

	//MARK: Configure
	private func configure() {
		action.addTarget(self, action: #selector(actTapAction(_:)), for: .touchUpInside)
	}

	//MARK: Action
	@objc private func actTapAction(_ sender: UIButton)  {
		actionCallback?(self, sender)
	}
}


//MARK: ViewDisplayable
extension SimpleErrorView {


	public var viewTransitionDuration: Double {
		return 0.3
	}
	
	/// Add view and display view on view controller
	///
	/// - Parameters:
	///   - viewController: controller in which view will be displayed
	///   - insets: margins from edges
	///   - animated: with fade animation if true
	///   - completion: callback when view is displayed
	public func show(fromViewController viewController: UIViewController, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

		add(to: viewController.view, withInsets: insets)
		presentView(animated: animated) { (finished) in
//			if finished{
//				self?.playAnimation()
//			}
			completion?(finished)
		}

	}

	/// Add view and display view on another view
	///
	/// - Parameters:
	///   - view: parent view on which view will be displayed
	///   - insets: margins from edges
	///   - animated: with fade animation if true
	///   - completion: callback when view is displayed
	public func show(fromView view: UIView, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

		add(to: view, withInsets: insets)
		presentView(animated: animated) {  (finished) in
//			if finished{
//				self?.playAnimation()
//			}
			completion?(finished)
		}
	}


	/// Hide and remove view from superview
	///
	/// - Parameters:
	///   - animated: with fade animation if true
	///   - completion: callback when view is hidden and removed
	public func hide(animated: Bool, completion: ((Bool) -> Void)?) {
//		self.stopAnimation()
		self.dismissView(animated: animated, completion: completion)
	}
}
