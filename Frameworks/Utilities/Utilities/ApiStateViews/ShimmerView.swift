//
//  ShimmerView.swift
//  TemplateProject
//
//  Created by Yasir Ali on 25/10/2018.
//  Copyright © 2018 Yasir Ali. All rights reserved.
//

import UIKit

/*
// Display shimmer animation with a background image
final public class ShimmerView: UIView, ViewAnimatable {

	//MARK: View Animatable

	/// Animation duration for showing and hiding view if animated is true while calling show or hide func
	public var animationDuration: Double = 0.3

	public var isAnimating: Bool {
		return shimmerView.isShimmering
	}


	/// Start Shimmer animation
	public func playAnimation() {
		shimmerView.isShimmering = true
	}


	/// Stop Shimmer animation
	public func stopAnimation() {
		shimmerView.isShimmering = false
	}

	//MARK: Properties
	private let  imageView: UIImageView = {
		let imgView = UIImageView(frame: .zero)
		imgView.contentMode = .scaleAspectFit
		return imgView
	}()

//	private var shimmerView: FBShimmeringView = {
//		let shimmer = FBShimmeringView(frame: .zero)
//		return shimmer
//	}()

	public var image: UIImage?  = UIImage(named: "list-placeholder") {
		didSet{
			imageView.image = image
		}
	}

	//MARK: Lifecycle
	public override init(frame: CGRect) {
		super.init(frame: frame)
		customizeUI()
	}

    required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		customizeUI()
	}

	//MARK: UI

	/// Construct UI
	private  func customizeUI() {

		self.backgroundColor = UIColor.white

		shimmerView.contentView = imageView
		addSubViews(shimmerView)

		setupLayout()
	}

	/// Add Constraints to subviews
	private func setupLayout() {
		let parent = self
		NSLayoutConstraint.activate([
			shimmerView.topAnchor.constraint(equalTo: parent.topAnchor),
			shimmerView.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
			parent.trailingAnchor.constraint(equalTo: shimmerView.trailingAnchor),
			parent.bottomAnchor.constraint(equalTo: shimmerView.bottomAnchor)
			])
	}
}

//MARK: ViewDisplayable
extension ShimmerView: ViewDisplayable {

	public var viewTransitionDuration: Double {
		return 0.3
	}

	/// Add view and display view on view controller
	///
	/// - Parameters:
	///   - viewController: controller in which view will be displayed
	///   - insets: margins from edges
	///   - animated: with fade animation if true
	///   - completion: callback when view is displayed
	public func show(fromViewController viewController: UIViewController, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

		add(to: viewController.view, withInsets: insets)
		presentView(animated: animated) { [weak self] (finished) in
			if finished{
				self?.playAnimation()
			}
			completion?(finished)
		}

	}

	/// Add view and display view on another view
	///
	/// - Parameters:
	///   - view: parent view on which view will be displayed
	///   - insets: margins from edges
	///   - animated: with fade animation if true
	///   - completion: callback when view is displayed
	public func show(fromView view: UIView, insets: UIEdgeInsets, animated: Bool, completion: ((Bool) -> Void)?) {

		add(to: view, withInsets: insets)
		presentView(animated: animated) { [weak self] (finished) in
			if finished{
				self?.playAnimation()
			}
			completion?(finished)
		}
	}


	/// Hide and remove view from superview
	///
	/// - Parameters:
	///   - animated: with fade animation if true
	///   - completion: callback when view is hidden and removed
	public func hide(animated: Bool, completion: ((Bool) -> Void)?) {
		self.stopAnimation()
		self.dismissView(animated: animated, completion: completion)
	}
}

*/
