//
//  String+extension.swift
//  Utilities
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation

extension String {
    /// Check if string is valid according to regular expression
    ///
    /// - Parameter exp: regular expression
    /// - Returns: validation status of text
    func isValid(forExp exp: String) -> Bool {
        guard let test = NSPredicate(format:"SELF MATCHES %@", exp) as NSPredicate? else { return false }
        return test.evaluate(with: self)
    }

}
