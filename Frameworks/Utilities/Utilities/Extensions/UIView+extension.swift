//
//  UIView+extension.swift
//  Utilities
//
//  Created by Yasir ali on 01/12/2021.
//

import UIKit

public extension UIView {


    /// Add subviews to parent view and set translatesAutoresizingMaskIntoConstraints to false
    ///
    /// - Parameter subviews: list of subviews to be added in parent view
    func addSubViews(_ subviews: UIView...) {
        for subview in subviews {
            subview.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(subview)
        }
    }
    
}
