//
//  Validators.swift
//  Utilities
//
//  Created by Yasir Ali on 30/11/2021

import Foundation



/// Validates Email
class EmailValidator: BaseValidator {
    override func validate(text: String) -> ValidationState {
        if text.isEmpty {
            return .empty
        }else if text.isValid(forExp: ValidatorType.email.rawValue) {
            return .valid
        }else {
            return .invalid(invalidStateMessage(defaultMessage: "Invalid Email"))
        }
    }
}


/// Validates Password
class PasswordValidator: BaseValidator {

    override func validate(text: String) -> ValidationState {
        if text.isEmpty {
            return .empty
        }else if text.isValid(forExp: ValidatorType.password.rawValue) {
            return .valid
        }else {
            return .invalid(invalidStateMessage(defaultMessage: "Invalid Password"))//"Password wrong format is entered")) //"Incorrect Password"))
        }
    }
}

/// Validates Name
class NotEmptyValidator: BaseValidator {
    override func validate(text: String) -> ValidationState {
        if text.isEmpty {
            return .empty
        }
        else if /*(text.isValid(forExp: ValidatorType.notEmpty.rawValue) && */ !text.isReallyEmpty //)
        {
            return .valid
        }else {
            return .invalid(invalidStateMessage(defaultMessage: "Invalid Field"))
        }
    }
}

/// Validates Name
class NameValidator: BaseValidator {
    override func validate(text: String) -> ValidationState {
        if text.isEmpty {
            return .empty
        }else if text.isValid(forExp: ValidatorType.name.rawValue) {
            return .valid
        }else {
            return .invalid(invalidStateMessage(defaultMessage: "Invalid Name"))
        }
    }
}



/// Validates Phone
class PhoneValidator: BaseValidator {
    override func validate(text: String) -> ValidationState {
        let firstFour = String(text.prefix(4))
        if text.isEmpty || text == "+92" {
            return .empty
        }else if (text.isValid(forExp: ValidatorType.phoneNo.rawValue) && firstFour == "+923") {
            return .valid
        }else {
            return .invalid(invalidStateMessage(defaultMessage: "Invalid Mobile Number (+923212496812)"))//"Invalid Mobile No"))
        }
    }
}

extension String {
    var isReallyEmpty: Bool {
        //return self.trimmingCharacters(in: .whitespaces).isEmpty
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    func isEmptyOrWhitespace() -> Bool {

        if(self.isEmpty) {
            return true
        }

        return (self.trimmingCharacters(in: .whitespaces) == "")
    }
    
    public func removeAllWhiteSpaces() -> String {
        let stringWithoutAnySpace = String(self.filter { !" \n\t\r".contains($0) })
        return stringWithoutAnySpace
    }
}
