//
//  ValidationFactory.swift
//  Utilities
//
//  Created by Yasir Ali on 30/11/2021.

import Foundation
import RxSwift


/// Available Types of Validators
///
/// - email: verify valid email using regx [A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}
/// - password: verify valid pass with 8 characters using .{8,}
/// - notEmpty: verify valid not empty text using .{1,}
/// - phoneNo: verify valid phoneNo with 12 digits using ^\\d{12}$
public enum ValidatorType: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    case password = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$" //--> (Minimum 8 and Maximum 20 characters at least 1 Alphabet, 1 Number and 1 Special Character) //".{8,}"
    case notEmpty = ".{1,}"
    /// nameWithWhiteSpaces
    case name = "[a-zA-Z].*[a-zA-Z]{1,50}" //"[a-zA-Z].*[a-zA-Z]{2,50}" // ([a-zA-Z].*[a-zA-Z]{1,50}) //"[a-zA-Z]+(\\s+[a-zA-Z]+)*"
    case phoneNo = "^[0-9+]{0,1}+[0-9]{12,12}$" //"^\\d{12}$"
}

public enum ValidatorTypeSecondary: String {
    case notEmpty = ".{1,}"
}

public enum VaildatorFactory {
    public static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
        case .notEmpty: return NotEmptyValidator()
        case .phoneNo: return PhoneValidator()
        case .name: return NameValidator()
        }
    }
}
