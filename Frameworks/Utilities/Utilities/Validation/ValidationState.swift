//
//  ValidationState.swift
//  Utilities
//
//  Created by Yasir Ali on 30/11/2021.
//

import Foundation


public enum ValidationState {
    case empty
    case inprocess
    case valid
    case invalid(String?)
}

extension ValidationState: Equatable {

    public static func == (lhs: ValidationState, rhs: ValidationState) -> Bool {
        switch (lhs,rhs) {
        case (.empty,.empty):
            return true
        case (.inprocess,.inprocess):
            return true
        case (.valid,.valid):
            return true
        case (.invalid(let lhsMsg),.invalid(let rhsMsg)):
            return lhsMsg == rhsMsg
        default:
            return false
        }
    }
}
