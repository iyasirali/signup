//
//  ValidatorProtocols.swift
//  Utilities
//
//  Created by Yasir Ali on 30/11/2021.

import Foundation
import RxSwift
import RxCocoa


/// A Type that validates text
public protocol ValidatorConvertible {


    /// binded to source
    var text: PublishSubject<String> { get }

    /// represents state of text
    var state: BehaviorSubject<ValidationState> { get }

    var invalidMessage: String { get set }

    /// Validates text
    ///
    /// - Parameter text: string to be validated
    /// - Returns: Validation state of string
    func validate(text: String) -> ValidationState
}


/// BaseClass which implementes ValidatorConvertible. All Other Validators inherit from BaseValidator

/**
Observe text, calls validate func on text change and update state.
*/

class BaseValidator: ValidatorConvertible {

    private let disposeBag = DisposeBag()
    let text = PublishSubject<String>()
    ///private state
    private let _state = PublishSubject<ValidationState>()
    public let state = BehaviorSubject<ValidationState>(value: .empty)
    public var invalidMessage: String = ""

    /// Initialze validator
    public init() {

        ///Observe text changes and bind to state
        text.map { [weak self] (input) -> ValidationState in
            return self?.validate(text: input) ?? .empty
            }.bind(to: _state).disposed(by: disposeBag)

        ///bind private property _state to state which is available to consumer
        _state.bind(to: state).disposed(by: disposeBag)
    }


    /// Initialize validator with custom invalid message
    ///
    /// - Parameter invalidMessage: message for invalid state
    public convenience init(invalidMessage: String) {
        self.init()
        self.invalidMessage = invalidMessage
    }


    /// Initialize validator with provided source
    ///
    /// - Parameter source: string which needs to be validated
    public convenience init(source: Observable<String>) {
        self.init()

        source.bind(to: text).disposed(by: disposeBag)
    }

    /// bind source to text
    public func set(source: Observable<String>) {
        source.bind(to: text).disposed(by: disposeBag)
    }

    /// Default Implementation for validate which must be overrides in child classes
    func validate(text: String) -> ValidationState {
        return .empty
    }

    func invalidStateMessage(defaultMessage: String) -> String {
        if invalidMessage.isEmpty {
            return defaultMessage
        }else {
            return invalidMessage
        }
    }
}

