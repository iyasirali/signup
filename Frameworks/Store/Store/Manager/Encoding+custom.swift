//
//  Encoding+custom.swift
//  Store
//
//  Created by Yasir Ali on 30/11/2021.


import Foundation

public extension Encodable{

    var jsonEncodedData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: [String:Any] {
        guard let data =  jsonEncodedData else {
            return [:]
        }
        guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [])  else {
            return [:]
        }
        guard let dictionary =  jsonObject as? [String:Any] else {
            return [:]
        }
        return dictionary
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
