//
//  NetworkManager.swift
//  Store
//
//  Created by Yasir Ali on 30/11/2021.


import Foundation
import RxSwift
import Moya

import Alamofire


public class NetworkManager {
    

  //  http://restapi.adequateshop.com/api/authaccount/registration
    
    public let baseUrl = URL(string: "http://restapi.adequateshop.com/api/authaccount/")!
    public static let shared = NetworkManager()
    public var customHeaders: [String:String] = [:] //["ApiKey":"099280a3038f6abd"]
    
    /// subscibe to this property to bring user to login screen
    public  let userNotLogin = PublishSubject<Void>()
    
    let provider = MoyaProvider<ApiEndPoint>()
    let disposeBag = DisposeBag()
    
    
    private init() {
        
    }
    
    
    /// Create new request and return observable of response model
    ///
    /// - Parameters:
    ///   - method: HTTP method
    ///   - task: task that will be executed
    ///   - path: url path which will be appended at the end of baseurl
    ///   - headers: request headers
    /// - Returns: Single(Observable Sequence) of  Reponse Model
    func newRequest<T>(method: Moya.Method, task: Task, path: String, headers: [String: String]? = nil) -> PrimitiveSequence<SingleTrait,T> where T: Decodable  {
        let mergedHeaders = add(customHeaders: customHeaders, toRequestHeaders: headers ?? [:])
        let endPoint = ApiEndPoint(baseUrl: baseUrl, customHeaders: mergedHeaders, path: path, method: method, task: task)
        return request(endPoint: endPoint)
    }
    
    func newRequestForMultipart<T>(method: Moya.Method, task: Task, path: String, headers: [String: String]? = nil) -> PrimitiveSequence<SingleTrait,T> where T: Decodable  {
        let mergedHeaders = add(customHeaders: customHeaders, toRequestHeaders: headers ?? [:])
        
       // let newBaseUrl = URL(string: "google.com")!
        let endPoint = ApiEndPoint(baseUrl: baseUrl, customHeaders: mergedHeaders, path: path, method: method, task: task)
        return request(endPoint: endPoint)
    }
    
    
    /// Create new request, filter success status codes and map json to response model
    ///
    /// - Parameter endPoint: contains apiEndPoint
    /// - Returns: Single(Observable Sequence) of  Reponse Model
    private func request<T>(endPoint: ApiEndPoint) -> PrimitiveSequence<SingleTrait,T> where T: Decodable {
        let request =  provider.rx.request(endPoint)
        return request.flatMap { (response) -> PrimitiveSequence<SingleTrait, T> in
            return Observable.create { [weak self] (observer) -> Disposable in
                do {
                    let data = try response.filterSuccessfulStatusCodes()
                    
                    if T.self != NoResponse.self {
                        let model = try data.map(T.self)
                        observer.onNext(model)
                    }else {
                        observer.onNext(NoResponse() as! T)
                    }
                }catch {
                    
                    if let moyaError = self?.parseError(error: error,endPoint: endPoint) {
                        observer.onError(moyaError)
                    }else {
                        observer.onError(error)
                    }
                }
                
                observer.onCompleted()
                return Disposables.create()
                }.asSingle()
        }
    }
    
    private func parseError(error: Error, endPoint: ApiEndPoint) -> MoyaError? {
        print("parseError called")
        guard let mError = error as? MoyaError else {
            let nsError = NSError(domain: "error.parsetomoya.failed", code: -1, userInfo: [NSLocalizedDescriptionKey : "Server is unable to handle request"])
            return MoyaError.underlying(nsError, nil)
        }
        
        let statusCode = mError.response?.statusCode ?? 0
        
        // user isn't logged in
        if statusCode == 401 {
            userNotLogin.onNext(())
        }
        
        switch mError {
        case .objectMapping(let error, let errorRes):
            print(error.localizedDescription)
        case .underlying(let underlyingError, _):
            print("underlying error \(underlyingError.localizedDescription)")
        // po error as? DecodingError
        default:
            break
        }

        
        //custom error
         if let customErrorOptional = try? mError.response?.map(ServerCustomError.self){ //customErrorOptional.errors?.first {
            let customError = customErrorOptional.Message ?? ""
            return MoyaError.underlying(LMSError.customError(customError: customErrorOptional), mError.response)
        }
        //internal server error
        else if statusCode == 500 {
            let nsError = NSError(domain: "error.internalserver", code: 500, userInfo: [NSLocalizedDescriptionKey : "Unfortunately, server error occurred"]) //"Server is not responding"])
            return MoyaError.underlying(nsError, nil)
        }else {
            return mError
        }
    }
    
    /// Combine customHeaders and request headers if a key exist in both request header will be selected
    ///
    /// - Parameters:
    ///   - customHeaders: headers in networking manager e.g session token
    ///   - request: request specific header sent in request object
    /// - Returns: merge of custom and request headers
    private func add(customHeaders: [String:String], toRequestHeaders requestHeaders: [String:String]) -> [String:String] {
        var newHeaders = [String:String]()
        
        //Custom Headers
        for (header, value) in customHeaders {
            newHeaders[header] = value
        }
        
        //Request Headers
        for (header, value) in requestHeaders {
            newHeaders[header] = value
        }
        
        return newHeaders
    }
}



extension NetworkManager {
    func convertToDictionary(data: Data?) -> [String: Any]? {
        if let data = data {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                // Handle Error
            }
        }
        return nil
    }
}



public class NetworkManagerDownloader {
    

    
}

// MARK: - Helpers
private extension String {
    var urlEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8EncodedData: Data {
        return self.data(using: .utf8)!
    }
}
public enum LMSError: Error {
    
    //case customError(dic: [String:Any])
    case customError(customError: ServerCustomError)
}
