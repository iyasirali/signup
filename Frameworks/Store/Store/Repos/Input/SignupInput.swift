//
//  SignupInput.swift
//  Store
//
//  Created by Yasir ali on 30/11/2021.
//

public class SignupInput: Encodable {
    
    public var email: String
    public var phone: String
    public var name: String
    
    private var password = 123456 // for dummy api
    
    public  init(email: String, phone: String, name: String) {
        self.email = email
        self.phone = phone
        self.name = name
    }
    
    enum CodingKeys: String, CodingKey {
        case email
       // case phone
        case name
        case password
    }
    
    // Encodable protocol methods
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(email, forKey: .email)
        try container.encodeIfPresent(password, forKey: .password)
        try container.encodeIfPresent(name, forKey: .name)
    }
}
