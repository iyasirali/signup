//
//  ServorCustomError.swift
//  Store
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation

open class ServerCustomError: Decodable {
   
    public var Message: String?
}
