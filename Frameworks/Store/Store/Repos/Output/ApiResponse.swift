//
//  ApiResponse.swift
//  Store
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation

public enum ModelType<T> {
    case registerationResponse(T)
}

public struct APIResponse<T : Decodable>: Decodable  {
    
   
    public var message: String?
    public var data: T?
    public var Message: String?
    
}
