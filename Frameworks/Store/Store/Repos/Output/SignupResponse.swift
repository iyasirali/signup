//
//  SignupResponse.swift
//  Store
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation

public class SignupResponse: Decodable {
    
    public var Name: String?
    public var Email: String?
    public var Token: String?
    public var Id: Int?
   
    
    public  init() {}
    
    enum CodingKeys: String, CodingKey {
        case Name
        case Email
        case Token
        case Id
    }
    
//    // Encodable protocol methods
//    public func encode(to encoder: Encoder) throws {
//        
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        
//        try container.encodeIfPresent(Name, forKey: .Name)
//        try container.encodeIfPresent(Email, forKey: .Email)
//        try container.encodeIfPresent(Token, forKey: .Token)
//        try container.encodeIfPresent(Id, forKey: .Id)
//    }
    
    // Decodable protocol methods
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        Name = try container.decodeIfPresent(String.self, forKey: .Name) ?? ""
        Email = try container.decodeIfPresent(String.self, forKey: .Email) ?? ""
        Token = try container.decodeIfPresent(String.self, forKey: .Token) ?? ""
        Id = try container.decodeIfPresent(Int.self, forKey: .Id) ?? 0
    }
}
