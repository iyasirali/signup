//
//  AuthRepo.swift
//  Store
//
//  Created by Yasir ali on 30/11/2021.
//

import Foundation
import RxSwift
import Moya
import RxRelay

public protocol HasAuthRepo {
    var authRepo: AuthRepository { get }
}


public final class AuthRepository: AuthService {
    
    let remote: AuthService
    
    public init(remote: AuthService) {
        self.remote = remote
    }
    
    public func signup(input: SignupInput) -> PrimitiveSequence<SingleTrait, APIResponse<SignupResponse>> {
        remote.signup(input: input)
    }
    
}

public final class AuthRemoteSource: AuthService {
    
    public init() {}
    
    public func signup(input: SignupInput) -> PrimitiveSequence<SingleTrait, APIResponse<SignupResponse>> {
        let request: PrimitiveSequence<SingleTrait, APIResponse<SignupResponse>> = NetworkManager.shared.newRequest(method:
            .post, task: .requestParameters(parameters: input.json, encoding: JSONEncoding.default), path: "registration")
        return request
    }
    
}

public protocol AuthService {
    func signup(input: SignupInput) -> PrimitiveSequence<SingleTrait, APIResponse<SignupResponse>>
}

